/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:your hints/instructions,cplusplus.com for random syntax,
 * uses and functions, stackoverflow when that came up on google isntead(used just for syntax and function lookup)
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 4
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iostream>
using std::cout;
using std::endl;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";



int main(int argc, char *argv[])
{
	// define long options
	static int byteonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				byteonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	enum states {ws,nws};
	char in;
	string wordHold="";
	set<string> uwords;
	int bytec=0,wordc=0,linec=0,state=0,length=0,maxLength=0,uwordsc=0;

	while (fread(&in,1,1,stdin)) {
    if(in==' '){
			if(state==nws){
				uwords.insert(wordHold);
				wordHold="";
			}
			state=ws;
		}
		else if(in=='\n'){
			if(state==nws){
				uwords.insert(wordHold);
				wordHold="";
			}
			if(length>maxLength) maxLength=length;
			length=-1;
			linec++;
			state=ws;
			}
		else if(in=='\t'){
			if(state==nws){
				uwords.insert(wordHold);
				wordHold="";
			}
			state=ws;
			length+=7-length%8;
		}
		else{
			if(state==ws){
				wordc++;
			}
			state=nws;
			wordHold.push_back(c);
		}

		length++;
		bytec++;
	}
	uwordsc=uwords.size()-1;
	//print
	if(linesonly) cout<<linec<<"\t";
	if(wordsonly) cout<<wordc<<"\t";
	if(byteonly) cout<<bytec<<"\t";
	if(longonly) cout<<maxLength<<"\t";
	if(uwordsonly) cout<<uwordsc<<endl;
	if(uwordsonly==0&&longonly==0&&byteonly==0&&wordsonly==0&&linesonly==0)
		cout<<linec<<"\t"<<wordc<<"\t"<<bytec<<endl;
	return 0;
}