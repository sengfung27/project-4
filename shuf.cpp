#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=0;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
					return 0;
					} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */
		srand(time(0));

		if (userange){
			size_t urange=rhigh-rlow+1;
			vector<int> vrange;
			vector<int> ranvrange(urange);
			for(size_t i=rlow;i<rhigh+1;i++) vrange.push_back(i);
			for(size_t i=0;i<urange;i++){
				int randnum=rand()%urange;
				while (ranvrange[randnum]!=0) randnum=rand()%urange;
				ranvrange[randnum]=vrange[i];
			}
			size_t maxline=0;
			if (count) maxline=count;
			else maxline = urange;
			//print
			for(size_t i=0;i<urange;i++) cout<<ranvrange[i]<<endl;
			}

		else{
			vector<string> v1;
			string readLine;
			if(echo){
				while (optind < argc) v1.push_back(argv[optind++]);
			}
			else{
				while(getline(cin,readLine)) v1.push_back(readLine);
			}

			vector<string> v2(v1.size());
			//take from v1 and put in v2 in random order
			for(size_t i=0;i<v1.size();i++){
				int randnum=rand()%v1.size();
				while (v2[randnum]!=""){
					randnum=rand()%v1.size();
			}
			v2[randnum]=v1[i];
			}
			size_t maxline=0;
			if (count) maxline=count;
			else{ maxline = v2.size();}
			//print
			for(size_t i=0;i!=maxline;i++) cout<<v2[i]<<endl;
		}

	return 0;
}
