#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
using namespace std;
#include <vector>
using std::vector;
#include <algorithm>


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	string line;
	set<string> s1;
	vector<string> strvec;
	set<string, igncaseComp> s2;
	multiset<string, igncaseComp> ms2;
	multiset<string> ms1;


	while(getline(cin,line)){
		strvec.push_back(line);
		s1.insert(line);
		s2.insert(line);
		ms1.insert(line);
		ms2.insert(line);

		}
	int size= strvec.size();
	for(int i = 0; i < strvec.size() ; i++)
	{
		int min = i;
		for(int j= i + 1; j < size ;  j ++)
		{
			if(strvec[min] > strvec[j])
			{
				min = j;
			}
		}
		swap(strvec[i],strvec[min]);
	}


	if((descending==0) && (unique==0) && (ignorecase==0))
	{
				for(int i = strvec.size(); i >= 1 ; i--)
				{
					ms1.insert(strvec[i]);
				}
				for(multiset<string>::iterator i = ms1.begin(); i !=ms1.end				 (); i++)
				{
					cout<<*i<<endl;
				}
	}
	else if(descending==1)
	{
		if((unique==1) && (ignorecase==1))
		{
			for(int i = 0 ; i < strvec.size() ; i++)
			{
				s2.insert(strvec[i]);
			}
			for(set<string, igncaseComp>::iterator i =s2.begin(); i!=s2.end();i++)
			{
					cout<<*i<<endl;
			}
		}
		else if(unique==1)
		{
			for(int i = 0 ; i < strvec.size() ; i++)
			{
				s1.insert(strvec[i]);
			}
			for(set<string>::iterator i =s1.begin(); i!=s1.end();i++)
			{
					cout<<*i<<endl;
			}
		}
		else //ignorecase
		{
			for(int i = 0 ; i < strvec.size(); i++)
			{
				ms2.insert(strvec[i]);
			}
			for(multiset<string, igncaseComp>::iterator i = ms2.begin() ; i!=ms2.end() ; i++)
			{
				cout<<*i<<"\n";
			}
		}

	}
	//ascending
	else
		{
			if((unique==1) && (ignorecase==1))
		{
			for(int i = strvec.size() ; i >= 1 ; i--)
			{
				s2.insert(strvec[i]);
			}
			for(set<string, igncaseComp>::iterator i =s2.begin(); i!=s2.end();i++)
			{
					cout<<*i<<endl;
			}
		}
		else if(unique==1)
		{
			for(int i = strvec.size() ; i >= 1; i--)
			{
				s1.insert(strvec[i]);
			}
			for(set<string>::iterator i =s1.begin(); i!=s1.end();i++)
			{
					cout<<*i<<endl;
			}
		}
		else //ignorecase
		{
			for(int i = strvec.size() ; i >= 1; i++)
			{
				ms2.insert(strvec[i]);
			}
			for(multiset<string, igncaseComp>::iterator i = ms2.begin() ; i!=ms2.end() ; i++)
			{
				cout<<*i<<"\n";
			}
		}
		}



		return 0 ;
	}



//if(descending==1 && ignorecase==1 && unique==1){
	/* TODO: write me... */





